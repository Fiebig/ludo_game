package m�dn.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import m�dn.logic.GameLogic;

public interface StandardGameLoader {

	public static GameLogic load(InputStream in) throws IOException {
		try (ObjectInputStream data = new ObjectInputStream(in)) {
			return (GameLogic) data.readObject();
		}
		catch (ClassNotFoundException e) {
			throw new IOException(e);
		}
	}

	public static void save(GameLogic gl, OutputStream out) throws IOException {
		try (ObjectOutputStream save = new ObjectOutputStream(out)) {
			save.writeObject(gl);
		}
	}

}
