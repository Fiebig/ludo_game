package m�dn.ai;

import java.io.Serializable;

import m�dn.model.spieler.M�dnSpieler;
import m�dn.model.spieler.SpielFigur;

public interface Ai extends Serializable {
	/**
	 * Sucht eine sinnvolle Figur zum bewegen aus. Dabei wird nur aus Figuren
	 * gew�hlt, die mit aktueller W�rfelzahl auch bewegt werden k�nnen.
	 * 
	 * @param moveOptions
	 *            Map die, die m�glichen Figuren auf ihr Targetfeld mappt.
	 * @return Ausgew�hlte Figur
	 */
	public SpielFigur computeBestFigure(M�dnSpieler spieler);

}
