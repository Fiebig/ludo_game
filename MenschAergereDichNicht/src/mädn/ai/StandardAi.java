package m�dn.ai;

import gametheory.FiniteStrategyCombination;

import java.util.List;

import m�dn.model.spieler.M�dnSpieler;
import m�dn.model.spieler.SpielFigur;

public class StandardAi implements Ai {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public SpielFigur computeBestFigure(M�dnSpieler spieler) {
		List<SpielFigur> bestOptions = spieler
				.bestReplies(new FiniteStrategyCombination<M�dnSpieler, SpielFigur>());
		if (bestOptions == null) {
			return null;
		}
		int random = (int) (Math.random() * bestOptions.size());
		return bestOptions.get(random);
	}
}
