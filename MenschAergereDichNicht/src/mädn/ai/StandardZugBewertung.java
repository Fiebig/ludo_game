package m�dn.ai;

import gametheory.FiniteStrategyCombination;
import gametheory.ProfitFunction;
import m�dn.model.spielbrett.Feld;
import m�dn.model.spielbrett.Feldtyp;
import m�dn.model.spieler.M�dnSpieler;
import m�dn.model.spieler.SpielFigur;

public class StandardZugBewertung implements ProfitFunction<M�dnSpieler, SpielFigur> {

	private static final long serialVersionUID = 1L;

	@Override
	public double computeProfit(M�dnSpieler spieler,
			FiniteStrategyCombination<M�dnSpieler, SpielFigur> fsc) {
		SpielFigur sf = fsc.getStrategyForPlayer(spieler);
		Feld target = spieler.getGl().getRules().moveValidation(spieler, sf)
				.getTargetField();
		// Invalid Move
		if (target == null) {
			return Double.NEGATIVE_INFINITY;
		}
		/*
		 * 1. Priorit�t: Ins Haus ziehen
		 */
		if (target.getFeldtyp() == Feldtyp.ZIELFELD
				&& sf.getCurrentField().getFeldtyp() != Feldtyp.ZIELFELD) {
			return 600;
		}

		/*
		 * 2. Priorit�t: Gegner schmeissen
		 */
		if (target.getBesetzung() != null) {
			if (sf.getCurrentField().getFeldtyp() == Feldtyp.STARTFELD) {
				return 500;
			}
			return 400 - target.getBesetzung().getDistanceToHouse();
		}
		/*
		 * 3. Priorit�t: Aus dem Startbereich gehen
		 */
		if (sf.getCurrentField().getFeldtyp() == Feldtyp.STARTFELD) {
			return 300;
		}

		/*
		 * 4. Priorit�t: Figur au�er Reichweite bringen
		 */

		if (!sf.isNearlySaveOnField(sf.getCurrentField())
				&& sf.isNearlySaveOnField(target)) {
			return 200 - sf.getDistanceToHouse();
		}

		/*
		 * 5. Priorit�t: Figur nahe dem Haus oder im Haus bewegen
		 */
		return 100 - sf.getDistanceToHouse();
	}

}
