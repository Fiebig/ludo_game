package m�dn.logic;

import gametheory.ProfitFunction;

import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;

import m�dn.ai.Ai;
import m�dn.exceptions.IllegalGameStateException;
import m�dn.exceptions.IllegalPlayerStateException;
import m�dn.exceptions.M�dnException;
import m�dn.model.spielbrett.Spielbrett;
import m�dn.model.spielbrett.Startposition;
import m�dn.model.spieler.M�dnSpieler;
import m�dn.model.spieler.SpielFigur;
import m�dn.model.w�rfel.W�rfel;
import m�dn.rules.Rules;

/**
 * Die Klasse ObservableGameLogicDecorator implementiert die Schnittstelle
 * GameLogic und nutzt das Dekorierer-Muster um einer beliebigen Implementierung
 * von GameLogic Verhalten hinzuzuf�gen.
 * 
 * @author Alexander Fiebig
 * 
 */
public class ObservableGameLogicDecorator implements GameLogic {

	/**
	 * Zu nutzende GameLogic
	 */
	private final GameLogic gl;
	/**
	 * Liste der Listener, die �ber �nderungen am Spiel informiert werden m�ssen
	 */
	private final LinkedList<GameChangedListener> gcl;

	/**
	 * Initialisiert eine neue Instanz, die alle Aufrufe an delegate
	 * weiterleitet und die registrierten Listener ggf. dar�ber informiert.
	 * 
	 * @param delegate
	 *            Die zu verwendende GameLogic
	 * 
	 * @throws IllegalArgumentException
	 *             falls delegate selbst vom Typ ObservableGameLogicDecorator
	 *             ist
	 */
	public ObservableGameLogicDecorator(GameLogic delegate) {
		if (delegate instanceof ObservableGameLogicDecorator) {
			throw new IllegalArgumentException(
					"delegate darf nicht vom Typ ObservableGameLogicDecorator sein!");
		}
		this.gcl = new LinkedList<GameChangedListener>();
		this.gl = delegate;

	}

	/**
	 * Registriert einen Listener, sodass er in Zukunft �ber �nderungen am Spiel
	 * informiert wird. Ein Listener darf nur einmal registriert werden.
	 * 
	 * @param listener
	 *            Zu registrierender Listener
	 */
	public void addGameChangedListener(GameChangedListener listener) {
		if (!gcl.contains(listener)) {
			gcl.add(listener);
		}
	}

	/**
	 * Entfernt einen Listener, sodass er in Zukunft nicht mehr �ber �nderungen
	 * am Spiel informiert wird.
	 * 
	 * @param listener
	 *            Zu entfernender Listener
	 */
	public void removeGameChangedListener(GameChangedListener listener) {
		gcl.remove(listener);
	}

	@Override
	public void figurBewegen() {
		String zugMessage = "";
		try {
			gl.figurBewegen();
			zugMessage = getSpielerAmZug().getName() + " hat Figur bewegt!";
		}
		catch (M�dnException e) {
			zugMessage = "ERROR: " + e.getMessage();

		}
		finally {
			for (GameChangedListener listener : gcl) {
				listener.onChange(gl.getSpielbrett());
				listener.onChange(zugMessage);
				if (gl.gameFinished()) {
					listener.onChange(gl.getErgebnisListe());
				}

			}
		}
	}

	@Override
	public void w�rfeln() {
		String message = "";
		boolean w�rfelChanged = true;

		try {
			gl.w�rfeln();
		}
		catch (IllegalPlayerStateException | IllegalGameStateException e) {
			message = "ERROR: " + e.getMessage();
			w�rfelChanged = false;

		}
		finally {
			for (GameChangedListener listener : gcl) {
				if (w�rfelChanged == true) {
					listener.onChange(gl.getW�rfel());
				}
				else {
					listener.onChange(message);
				}
			}
		}
	}

	@Override
	public void zugBeenden() {
		String message = "";
		try {
			M�dnSpieler temp = gl.getSpielerAmZug();
			gl.zugBeenden();
			for (GameChangedListener listener : gcl) {
				listener.onChange(gl.getSpielerAmZug());
			}
			message = temp.getName() + " hat Zug beendet!\n";
			message += getSpielerAmZug().getName() + " ist an der Reihe!";
		}
		catch (IllegalPlayerStateException | IllegalGameStateException e) {
			message = "ERROR: " + e.getMessage();
		}
		finally {
			for (GameChangedListener listener : gcl) {
				listener.onChange(message);
			}
		}
	}

	@Override
	public Spielbrett getSpielbrett() {
		return gl.getSpielbrett();
	}

	@Override
	public M�dnSpieler getSpielerAmZug() {
		return gl.getSpielerAmZug();
	}

	@Override
	public List<M�dnSpieler> getPlayers() {
		return gl.getPlayers();
	}

	@Override
	public boolean gameFinished() {
		return gl.gameFinished();
	}

	@Override
	public List<M�dnSpieler> getErgebnisListe() {
		return gl.getErgebnisListe();
	}

	@Override
	public boolean isGameStarted() {
		return gl.isGameStarted();
	}

	@Override
	public void startGame() {
		String message = "";
		try {
			gl.startGame();
			for (GameChangedListener listener : gcl) {
				listener.onChange(gl.getSpielerAmZug());
			}
			message = "Spiel gestartet!\n";
			message += getSpielerAmZug().getName() + " ist an der Reihe!";
		}
		catch (IllegalGameStateException e) {
			message = "ERROR: " + e.getMessage();
		}
		finally {
			for (GameChangedListener listener : gcl) {
				listener.onChange(message);

			}
		}
	}

	@Override
	public Rules getRules() {
		return gl.getRules();
	}

	@Override
	public void addAiSpieler(String name, String farbe, Startposition sp,
			ProfitFunction<M�dnSpieler, SpielFigur> pf, Ai ai) {
		String message = "";
		try {
			gl.addAiSpieler(name, farbe, sp, pf, ai);
		}
		catch (IllegalGameStateException e) {
			message = "ERROR: " + e.getMessage();
		}
		finally {
			for (GameChangedListener listener : gcl) {
				listener.onChange(message);

			}
		}
	}

	@Override
	public void addMenschlichenSpieler(String name, String farbe,
			Startposition sp) {
		String message = "";
		try {
			gl.addMenschlichenSpieler(name, farbe, sp);
		}
		catch (IllegalGameStateException e) {
			message = "ERROR: " + e.getMessage();
		}
		finally {
			for (GameChangedListener listener : gcl) {
				listener.onChange(message);

			}
		}
	}

	@Override
	public void saveGame(OutputStream out) {
		String message = "";
		try {
			gl.saveGame(out);
			message = "Spiel wurde gespeichert!\n";
		}
		catch (IOException e) {
			message = "ERROR: " + e.getMessage();
		}
		finally {
			for (GameChangedListener listener : gcl) {
				listener.onChange(message);
			}
		}
	}

	@Override
	public W�rfel getW�rfel() {
		return gl.getW�rfel();
	}
}
