package m�dn.logic;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

import gametheory.FiniteGame;
import gametheory.ProfitFunction;
import m�dn.ai.Ai;
import m�dn.exceptions.IllegalGameStateException;
import m�dn.exceptions.IllegalPlayerStateException;
import m�dn.exceptions.M�dnException;
import m�dn.io.StandardGameLoader;
import m�dn.model.spielbrett.Spielbrett;
import m�dn.model.spielbrett.Startposition;
import m�dn.model.spieler.AiSpieler;
import m�dn.model.spieler.MenschlicherSpieler;
import m�dn.model.spieler.M�dnSpieler;
import m�dn.model.spieler.SpielFigur;
import m�dn.model.spieler.SpielerIngameStatus;
import m�dn.model.w�rfel.W�rfel;
import m�dn.operationsContainer.OperationsContainer;
import m�dn.rules.Rules;

public class StandardGameLogic extends FiniteGame<M�dnSpieler, SpielFigur>implements GameLogic {

	private static final long serialVersionUID = 1L;
	private final Spielbrett spielbrett;
	private final List<M�dnSpieler> ergebnisliste;
	private boolean gameStarted;
	private final Rules rules;
	private final W�rfel w�rfel;

	public StandardGameLogic(Rules rules, Spielbrett spielbrett) {
		this.spielbrett = spielbrett;
		this.ergebnisliste = new ArrayList<M�dnSpieler>();
		this.gameStarted = false;
		this.rules = rules;
		this.w�rfel = new W�rfel();
	}

	@Override
	public void addAiSpieler(String name, String farbe, Startposition sp, ProfitFunction<M�dnSpieler, SpielFigur> pf,
			Ai ai) throws IllegalGameStateException {
		if (gameStarted == true) {
			throw new IllegalGameStateException(
					"Es kann kein Spieler mehr hinzugef�gt werden, wenn das Spiel bereits gestartet ist!");
		}
		M�dnSpieler spieler = new AiSpieler(name, farbe, sp, this, pf, ai);
		if (players.contains(spieler)) {
			throw new IllegalGameStateException("Spieler kommen doppelt vor!");
		}
		players.add(spieler);
	}

	@Override
	public void addMenschlichenSpieler(String name, String farbe, Startposition sp) throws IllegalGameStateException {
		if (gameStarted == true) {
			throw new IllegalGameStateException(
					"Es kann kein Spieler mehr hinzugef�gt werden, wenn das Spiel bereits gestartet ist!");
		}
		M�dnSpieler spieler = new MenschlicherSpieler(name, farbe, sp, this);
		if (players.contains(spieler)) {
			throw new IllegalGameStateException("Spieler kommen doppelt vor!");
		}
		players.add(spieler);
	}

	@Override
	public void figurBewegen() throws M�dnException {
		if (gameStarted == false) {
			throw new IllegalGameStateException("Das Spiel muss gestartet worden sein um einen Zug auszuf�hren!");
		}
		if (gameFinished()) {
			throw new IllegalGameStateException("Das Spiel ist bereits zuende!");
		}
		spielerAmZug.figurBewegen();
		if (spielerAmZug.isFinished()) {
			ergebnisliste.add(spielerAmZug);
			if (gameFinished()) {
				ergebnisliste.add(getNextPlayer());
			}
		}
	}

	@Override
	public boolean gameFinished() {
		if (gameStarted == false) {
			return false;
		}
		return rules.gameFinished(this);
	}

	@Override
	public Spielbrett getSpielbrett() {
		return spielbrett;
	}

	@Override
	public List<M�dnSpieler> getPlayers() {
		return Collections.unmodifiableList(players);
	}

	@Override
	public void w�rfeln() throws IllegalGameStateException, IllegalPlayerStateException {

		if (gameStarted == false) {
			throw new IllegalGameStateException("Das Spiel muss gestartet worden sein um zu w�rfeln!");
		}
		if (gameFinished()) {
			throw new IllegalGameStateException("Das Spiel ist bereits zuende!");
		}
		spielerAmZug.wuerfle();
	}

	@Override
	public List<M�dnSpieler> getErgebnisListe() {
		return Collections.unmodifiableList(ergebnisliste);
	}

	private M�dnSpieler getNextPlayer() {
		M�dnSpieler n�chsterSpieler = spielerAmZug;
		do {
			n�chsterSpieler = OperationsContainer.circleGet(players, n�chsterSpieler, 1);
		}
		while (n�chsterSpieler.isFinished());
		return n�chsterSpieler;
	}

	@Override
	public boolean isGameStarted() {
		return gameStarted;
	}

	@Override
	public void startGame() throws IllegalGameStateException {
		if (gameStarted) {
			throw new IllegalGameStateException("Das Spiel wurde bereits gestartet!");
		}
		if (players.size() < 2 || players.size() > spielbrett.getSps().size()) {
			throw new IllegalGameStateException(
					"An einem Spiel m�ssen zwischen 2 und " + spielbrett.getSps().size() + " Spieler teilnehmen!");
		}
		this.spielerAmZug = players.get((int) (Math.random() * players.size()));
		spielerAmZug.setStatus(SpielerIngameStatus.W�RFELPHASE);
		this.gameStarted = true;
	}

	@Override
	public void zugBeenden() throws IllegalGameStateException, IllegalPlayerStateException {
		if (!gameStarted) {
			throw new IllegalGameStateException("Das Spiel wurde noch nicht gestartet!");
		}
		if (gameFinished()) {
			throw new IllegalGameStateException("Das Spiel ist bereits zuende!");
		}
		spielerAmZug.zugBeenden();
		spielerAmZug = getNextPlayer();
		spielerAmZug.setStatus(SpielerIngameStatus.W�RFELPHASE);
	}

	@Override
	public Rules getRules() {
		return rules;
	}

	@Override
	public void saveGame(OutputStream out) throws IOException {
		StandardGameLoader.save(this, out);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Game;;;" + spielbrett.getClass().getSimpleName() + ","
				+ rules.getClass().getSimpleName() + System.getProperty("line.separator"));
		for (ListIterator<M�dnSpieler> l = players.listIterator(); l.hasNext();) {
			sb.append(System.getProperty("line.separator") + l.next().toString());
		}
		return sb.toString();
	}

	@Override
	public W�rfel getW�rfel() {
		return w�rfel;
	}
}
