package m�dn.logic;

import gametheory.ProfitFunction;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import m�dn.ai.Ai;
import m�dn.exceptions.IllegalGameStateException;
import m�dn.exceptions.IllegalPlayerStateException;
import m�dn.exceptions.M�dnException;
import m�dn.model.spielbrett.Spielbrett;
import m�dn.model.spielbrett.Startposition;
import m�dn.model.spieler.M�dnSpieler;
import m�dn.model.spieler.SpielFigur;
import m�dn.model.w�rfel.W�rfel;
import m�dn.rules.Rules;

/**
 * Die Schnittstelle GameLogic enth�lt Methoden, die den Ablauf des Spiels
 * steuern.
 * 
 * @author Alexander Fiebig
 * 
 */
public interface GameLogic {
	/**
	 * 
	 * @return
	 */
	public M�dnSpieler getSpielerAmZug();

	/**
	 * 
	 * @return
	 */
	public List<M�dnSpieler> getPlayers();

	/**
	 * 
	 * @return
	 */
	public Spielbrett getSpielbrett();

	/**
	 * 
	 * @return
	 */
	public Rules getRules();

	/**
	 * 
	 * @return
	 */
	public W�rfel getW�rfel();

	/**
	 * 
	 * @return
	 */
	public List<M�dnSpieler> getErgebnisListe();

	/**
	 * 
	 * @return
	 */
	public boolean gameFinished();

	/**
	 * 
	 * @return
	 */
	public boolean isGameStarted();

	/**
	 * 
	 * @throws IllegalGameStateException
	 * @throws IllegalPlayerStateException
	 */
	public void zugBeenden() throws IllegalGameStateException,
			IllegalPlayerStateException;

	/**
	 * 
	 * @throws IllegalGameStateException
	 * @throws IllegalPlayerStateException
	 */
	public void w�rfeln() throws IllegalGameStateException,
			IllegalPlayerStateException;

	/**
	 * 
	 * @throws M�dnException
	 */
	public void figurBewegen() throws M�dnException;

	/**
	 * 
	 * @throws IllegalGameStateException
	 */
	public void startGame() throws IllegalGameStateException;

	/**
	 * 
	 * @param out
	 * @throws IOException
	 */
	public void saveGame(OutputStream out) throws IOException;

	/**
	 * 
	 * @param name
	 * @param farbe
	 * @param sp
	 * @throws IllegalGameStateException
	 */
	public void addMenschlichenSpieler(String name, String farbe,
			Startposition sp) throws IllegalGameStateException;

	/**
	 * 
	 * @param name
	 * @param farbe
	 * @param sp
	 * @param pf
	 * @param ai
	 * @throws IllegalGameStateException
	 */
	public void addAiSpieler(String name, String farbe, Startposition sp,
			ProfitFunction<M�dnSpieler, SpielFigur> pf, Ai ai)
			throws IllegalGameStateException;

}
