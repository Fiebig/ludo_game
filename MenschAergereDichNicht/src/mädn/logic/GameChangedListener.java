package m�dn.logic;

import java.util.List;

import m�dn.model.spielbrett.Spielbrett;
import m�dn.model.spieler.M�dnSpieler;
import m�dn.model.w�rfel.W�rfel;

public interface GameChangedListener {

	/**
	 * Informiert den Listener wenn ein Spieler das Spiel beendet.
	 * 
	 * @param ergebnisListe
	 *            Ver�nderte Ergebnisliste
	 */
	public void onChange(List<M�dnSpieler> ergebnisListe);

	/**
	 * Informiert den Listener �ber das aktuelle Spiel mithilfe von Nachrichten.
	 * 
	 * @param message
	 *            Spielnachricht
	 */
	public void onChange(String message);

	/**
	 * Informiert den Listener �ber Ver�nderungen am W�rfel.
	 * 
	 * @param w�rfel
	 *            Ver�nderter W�rfel
	 */
	public void onChange(W�rfel w�rfel);

	/**
	 * Informiert den Listener �ber Ver�nderungen auf dem Spielbrett.
	 * 
	 * @param spielbrett
	 *            Ver�ndertes Spielbrett
	 */
	public void onChange(Spielbrett spielbrett);

	public void onChange(M�dnSpieler currentSpieler);

}
