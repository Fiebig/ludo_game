package m�dn.gui;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import m�dn.model.spielbrett.KleinesSpielbrett;
import m�dn.model.spielbrett.Spielbrett;
import m�dn.model.spielbrett.StandardSpielbrett;

public class NewGameView extends Stage {

	private SpielerAuswahl spielerAuswahl;
	private final HBox hboxRb;
	private final VBox mainVb;
	private final ToggleGroup rbGroup;
	private final Button btn;

	public NewGameView() {
		Text txt1 = new Text("Spielbrett:");
		txt1.setFont(Font.font("Arial", FontWeight.BOLD, 16));
		this.rbGroup = new ToggleGroup();
		RadioButton rb1 = new RadioButton("Standard");
		rb1.setUserData(new StandardSpielbrett());
		rb1.setToggleGroup(rbGroup);
		rb1.setSelected(true);
		RadioButton rb2 = new RadioButton("Klein");
		rb2.setUserData(new KleinesSpielbrett());
		rb2.setToggleGroup(rbGroup);
		// because KleinesSpielbrett is not implemented yet
		rb2.setDisable(true);

		this.hboxRb = new HBox(20);
		hboxRb.getChildren().addAll(txt1, rb1, rb2);
		this.spielerAuswahl = new SpielerAuswahl(4);
		this.btn = new Button("Spiel erstellen");
		btn.setText("Spiel erstellen");
		this.mainVb = new VBox(50);
		mainVb.getChildren().addAll(hboxRb, spielerAuswahl, btn);
		mainVb.setPadding(new Insets(40, 0, 0, 40));
		ScrollPane root = new ScrollPane(mainVb);
		root.setStyle("-fx-background:F0FFF0;");
		this.setTitle("New Game");
		this.setScene(new Scene(root, 800, 600));
		this.sizeToScene();
		this.setResizable(false);
		this.toFront();
	}

	public ToggleGroup getRbGroup() {
		return rbGroup;
	}

	public SpielerAuswahl getSpielerAuswahl() {
		return spielerAuswahl;
	}

	public void setSpielerAuswahl(SpielerAuswahl spielerAuswahl) {
		this.spielerAuswahl = spielerAuswahl;
		mainVb.getChildren().setAll(hboxRb, spielerAuswahl, btn);
	}

	public Spielbrett getSelectedSpielbrett() {
		return (Spielbrett) rbGroup.getSelectedToggle().getUserData();
	}

	public Button getBtn() {
		return btn;
	}

}
