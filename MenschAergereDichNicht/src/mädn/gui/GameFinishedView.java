package m�dn.gui;

import java.util.List;
import java.util.ListIterator;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;
import m�dn.model.spieler.M�dnSpieler;

public class GameFinishedView extends Stage {

	public GameFinishedView(List<M�dnSpieler> ergebnisliste) {
		createStage(ergebnisliste);
	}

	private void createStage(List<M�dnSpieler> ergebnisliste) {
		this.setTitle("Ergebnis");

		TextFlow textFlow = new TextFlow();
		textFlow.setLayoutX(40);
		textFlow.setLayoutY(40);
		Text title = new Text("Game finished!\n");
		title.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.ITALIC,
				40));
		title.setFill(Color.RED);
		title.setStroke(Color.BLACK);
		textFlow.getChildren().add(title);
		for (ListIterator<M�dnSpieler> i = ergebnisliste.listIterator(); i
				.hasNext();) {
			Text text = new Text((i.nextIndex() + 1) + ". Platz: "
					+ i.next().getName() + "\n");
			text.setFont(Font.font("Arial", FontWeight.NORMAL, 30));
			textFlow.getChildren().add(text);
		}

		Group group = new Group(textFlow);
		Scene scene = new Scene(group, 700, 500, Color.WHITE);
		this.sizeToScene();
		this.setResizable(false);
		this.setScene(scene);
	}

}
