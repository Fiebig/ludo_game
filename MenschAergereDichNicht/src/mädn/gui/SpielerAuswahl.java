package m�dn.gui;

import javafx.geometry.Insets;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import m�dn.operationsContainer.OperationsContainer;

public class SpielerAuswahl extends VBox {

	private final HBox[] hboxes;

	public SpielerAuswahl(int rowCount) {
		Text hl1 = new Text("Spielertyp");
		Text hl2 = new Text("Name");
		Text hl3 = new Text("Farbe");
		hl1.setFont(Font.font("Arial", FontWeight.BOLD, 16));
		hl2.setFont(Font.font("Arial", FontWeight.BOLD, 16));
		hl3.setFont(Font.font("Arial", FontWeight.BOLD, 16));
		HBox hboxHead = new HBox(0);
		hboxHead.getChildren().addAll(hl1, hl2, hl3);
		HBox.setMargin(hl1, new Insets(0, 0, 0, 93));
		HBox.setMargin(hl2, new Insets(0, 0, 0, 145));
		HBox.setMargin(hl3, new Insets(0, 0, 0, 162));
		this.hboxes = new HBox[rowCount];
		initHbArray();
		this.setSpacing(50);
		this.getChildren().addAll(hboxHead);
		this.getChildren().addAll(hboxes);
	}

	private void initHbArray() {
		for (int i = 0; i < hboxes.length; i++) {
			// Spieler HBox
			Text t = new Text("Spieler " + (i + 1) + ":");
			t.setFont(Font.font("Arial", FontWeight.BOLD, 16));

			final ComboBox<CbItems> cb = new ComboBox<>();
			cb.setEditable(false);
			cb.setMinSize(200, 20);
			cb.setVisibleRowCount(3);
			cb.getItems().addAll(CbItems.MENSCHLICHER_SPIELER,
					CbItems.AI_SPIELER, CbItems.INAKTIV);
			cb.setValue(CbItems.INAKTIV);

			final TextField tf = new TextField();
			tf.setText("sp" + (i + 1));
			tf.setMinSize(120, 20);

			final ColorPicker cp = new ColorPicker();
			cp.setValue(OperationsContainer.randomColor());

			hboxes[i] = new HBox(20);
			hboxes[i].getChildren().addAll(t, cb, tf, cp);
		}
	}

	public String getTextFieldContent(int row) {
		TextField temp = (TextField) hboxes[row].getChildren().get(2);
		return temp.getText();
	}

	public CbItems getComboBoxSelection(int row) {
		@SuppressWarnings("unchecked")
		ComboBox<CbItems> temp = (ComboBox<CbItems>) hboxes[row].getChildren()
				.get(1);
		return temp.getValue();
	}

	public Color getColorPickerSelection(int row) {
		ColorPicker temp = (ColorPicker) hboxes[row].getChildren().get(3);
		return temp.getValue();
	}

	public int getRowCount() {
		return hboxes.length;
	}

	enum CbItems {
		INAKTIV, MENSCHLICHER_SPIELER, AI_SPIELER
	}
}