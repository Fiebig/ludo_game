package m�dn.gui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import m�dn.control.ThreadingGameController;
import m�dn.exceptions.IllegalGameStateException;
import m�dn.io.StandardGameLoader;
import m�dn.logic.GameLogic;
import m�dn.logic.ObservableGameLogicDecorator;
import m�dn.model.spielbrett.Feld;
import m�dn.model.spieler.AiSpieler;
import m�dn.model.spieler.M�dnSpieler;

/**
 * Startet die graphische Benutzeroberfl�che von M�dn.
 * 
 * @author Alexander Fiebig
 * 
 */
public class MainController {
	// Model
	private ThreadingGameController gc;
	// View
	private final MainView view;

	/**
	 * Erstellt ein neues M�dn-Spiel. Es wird ein StandardSpielbrett erstellt.
	 */
	public MainController(MainView view) {
		this.view = view;
		view.getNewGameItem().setOnAction(new NewGameEventHandler());
		view.getLoadGameItem().setOnAction(new LoadGameEventHandler());
		view.getSaveGameItem().setOnAction(new SaveGameEventHandler());
		view.getStartGameItem().setOnAction(new StartGameEventHandler());
		view.getQuitGameItem().setOnAction(e -> Platform.exit());
		view.getW�rfelBtn().setOnAction(new W�rfelBtnEventHandler());
		view.getFigurBewegenBtn().setOnAction(new FigurBewegenBtnEventHandler());
		view.getZugBeendenBtn().setOnAction(new ZugBeendenBtnEventHandler());
		view.getTimeSlider().valueProperty().addListener(new TimeSliderListener());
	}

	public void showMainView() {
		view.show();
	}

	class NewGameEventHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			try {
				NewGameController ngc = new NewGameController(new NewGameView());
				// showAndWait
				ngc.showAndWaitNewGameView();
				// Infos holen
				GameLogic newGl = ngc.getGl();
				if (newGl != null) {
					// restores
					if (gc != null) {
						gc.terminateAiThread();
					}
					// Controller setten
					gc = new ThreadingGameController(new ObservableGameLogicDecorator(newGl));
					gc.addGameChangedListener(view);
					gc.setAiTimeout((int) view.getTimeSlider().getValue());
					// Welt zeichnen
					view.getWorldPane().initForGame(gc.getSpielbrett(), gc.getSpielerList());
					for (Feld feld : gc.getSpielbrett().getAllFields()) {
						view.getWorldPane().setFieldAction(feld, new FieldClickedEventHandler(feld));
					}
					view.getStatusText().appendText("Ein neues Spiel wurde erstellt!\n");
					// Spielerfarben Infos anzeigen
				}
			}
			catch (IllegalGameStateException e) {
				view.getStatusText().appendText("ERROR: " + e.getMessage() + "\n");
			}
		}
	}

	class LoadGameEventHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			try {
				if (gc != null) {
					gc.setPauseAiThread(true);
				}
				FileChooser fileChooser = new FileChooser();
				fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
				File file = fileChooser.showOpenDialog(new Stage());
				if (file != null) {
					GameLogic gl = StandardGameLoader.load(new FileInputStream(file));
					// restores
					if (gc != null) {
						gc.terminateAiThread();
					}
					// Welt zeichnen
					view.getWorldPane().initForGame(gl.getSpielbrett(), gl.getPlayers());
					for (Feld feld : gl.getSpielbrett().getAllFields()) {
						view.getWorldPane().setFieldAction(feld, new FieldClickedEventHandler(feld));
					}
					// Kontroller erstellen
					gc = new ThreadingGameController(new ObservableGameLogicDecorator(gl));
					gc.addGameChangedListener(view);
					gc.setAiTimeout((int) view.getTimeSlider().getValue());
					gc.start();

					view.getW�rfelPane().drawW�rfel(gl.getW�rfel());
					view.getStatusText().appendText("Spiel wurde geladen!\n");
					view.getStatusText().appendText(gc.getSpielerAmZug().getName() + " ist am Zug!\n");
					// Spielerfarben Infos anzeigen
				}
			}
			catch (IOException e) {
				view.getStatusText().appendText("ERROR: " + e.getMessage() + "\n");
			}
			finally {
				if (gc != null) {
					gc.setPauseAiThread(false);
				}
			}
		}
	}

	class SaveGameEventHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(final ActionEvent e) {
			if (gc != null) {
				try {
					gc.setPauseAiThread(true);
					FileChooser fileChooser = new FileChooser();
					// Set extension filter
					FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("MADN_SAV files (*.sav)",
							"*.sav");
					fileChooser.getExtensionFilters().add(extFilter);
					fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
					// Create or select file
					File selectedFile = fileChooser.showSaveDialog(new Stage());
					if (selectedFile != null) {
						gc.saveGame(new FileOutputStream(selectedFile));
					}

				}
				catch (FileNotFoundException ex) {
					view.getStatusText().appendText("ERROR: " + ex.getMessage() + "\n");
				}
				finally {
					gc.setPauseAiThread(false);
				}
			}
			else {
				view.getStatusText().appendText("ERROR: Es ist noch kein Spiel erstellt worden!\n");
			}
		}
	}

	class StartGameEventHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			if (gc != null) {
				gc.start();
			}
			else {
				view.getStatusText().appendText("ERROR: Es ist noch kein Spiel erstellt worden!\n");
			}
		}
	}

	class W�rfelBtnEventHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			if (gc != null) {
				if (gc.getSpielerAmZug() instanceof AiSpieler == false) {
					gc.wuerfle();
				}
			}
			else {
				view.getStatusText().appendText("ERROR: Es ist noch kein Spiel erstellt worden!\n");
			}
		}
	}

	class FigurBewegenBtnEventHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			if (gc != null) {
				if (gc.getSpielerAmZug() instanceof AiSpieler == false) {
					gc.figurBewegen();
				}
			}
			else {
				view.getStatusText().appendText("ERROR: Es ist noch kein Spiel erstellt worden!\n");
			}
		}
	}

	class ZugBeendenBtnEventHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			if (gc != null) {
				if (gc.getSpielerAmZug() instanceof AiSpieler == false) {
					if (gc.isGameStarted() && gc.getSpielerAmZug().getCurrentStrategy() != null) {
						view.getWorldPane()
								.clearSelectionEffect(gc.getSpielerAmZug().getCurrentStrategy().getCurrentField());
					}
					gc.zugBeenden();
				}
			}
			else {
				view.getStatusText().appendText("ERROR: Es ist noch kein Spiel erstellt worden!\n");
			}
		}
	}

	class TimeSliderListener implements ChangeListener<Number> {
		@Override
		public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
			if (gc != null) {
				gc.setAiTimeout(new_val.intValue());
			}
		}
	}

	class FieldClickedEventHandler implements EventHandler<MouseEvent> {
		private final Feld feld;

		public FieldClickedEventHandler(Feld feld) {
			this.feld = feld;
		}

		@Override
		public void handle(MouseEvent mev) {
			if (mev.getClickCount() == 1) {
				if (gc.isGameStarted() && gc.getSpielerAmZug() instanceof AiSpieler == false) {
					final M�dnSpieler aktuellerSpieler = gc.getSpielerAmZug();
					if (feld.getBesetzung() != null
							&& feld.getBesetzung().getRelatedPlayer().equals(aktuellerSpieler)) {
						if (aktuellerSpieler.getCurrentStrategy() != null) {
							view.getWorldPane()
									.clearSelectionEffect(aktuellerSpieler.getCurrentStrategy().getCurrentField());
						}
						aktuellerSpieler.setCurrentStrategy(feld.getBesetzung());
						view.getWorldPane().setSelectionEffect(feld);
					}
				}
			}
		}
	}

}