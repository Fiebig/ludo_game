package m�dn.gui;

import java.util.List;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;
import m�dn.logic.GameChangedListener;
import m�dn.model.spielbrett.Spielbrett;
import m�dn.model.spielbrett.StandardSpielbrett;
import m�dn.model.spieler.M�dnSpieler;
import m�dn.model.w�rfel.W�rfel;

public class MainView implements GameChangedListener {

	private final TextArea statusText;
	private final WorldPane worldPane;
	private final W�rfelPane w�rfelPane;
	private final TextFlow spielerTextFlow;
	private final MenuItem newGameItem;
	private final MenuItem loadGameItem;
	private final MenuItem saveGameItem;
	private final MenuItem startGameItem;
	private final MenuItem quitGameItem;
	private final Button w�rfelBtn;
	private final Button figurBewegenBtn;
	private final Button zugBeendenBtn;
	private final Stage primaryStage;
	private final Slider timeSlider;

	public MainView(Stage primaryStage) {
		this.worldPane = new WorldPane(800, new StandardSpielbrett());
		worldPane.setAlignment(Pos.CENTER);
		this.w�rfelPane = new W�rfelPane(100);
		this.statusText = new TextArea();
		statusText.setPromptText("StatusText");
		statusText.setPrefSize(500, 400);
		statusText.setEditable(false);
		this.newGameItem = new MenuItem("Neues Spiel");
		this.saveGameItem = new MenuItem("Spiel speichern");
		this.loadGameItem = new MenuItem("Spiel laden");
		this.startGameItem = new MenuItem("Spiel starten");
		this.quitGameItem = new MenuItem("Beenden");
		Menu menuSpiel = new Menu("Spiel");
		menuSpiel.getItems().addAll(newGameItem, startGameItem, loadGameItem, saveGameItem, quitGameItem);
		Menu menuHelp = new Menu("Hilfe");
		MenuBar menuBar = new MenuBar();
		menuBar.setPrefSize(1300, 40);
		menuBar.getMenus().addAll(menuSpiel, menuHelp);
		this.spielerTextFlow = new TextFlow();
		spielerTextFlow.setPrefSize(400, 60);
		spielerTextFlow.setTextAlignment(TextAlignment.LEFT);
		spielerTextFlow.toFront();
		Text txt = new Text("Am Zug:\n");
		txt.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.ITALIC, 25));
		spielerTextFlow.getChildren().addAll(txt, new Text(""));
		this.w�rfelBtn = new Button("W�rfeln");
		this.figurBewegenBtn = new Button("Figur bewegen");
		this.zugBeendenBtn = new Button("Zug beenden");
		HBox hbInVbRechts = new HBox(30);
		hbInVbRechts.getChildren().addAll(w�rfelBtn, w�rfelPane, figurBewegenBtn, zugBeendenBtn);
		this.timeSlider = new Slider(250, 2000, 1000);
		timeSlider.setMinSize(300, 40);
		timeSlider.setShowTickLabels(true);
		timeSlider.setMajorTickUnit(1750);
		Text txt2 = new Text("Ai Speed:");
		txt2.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.ITALIC, 14));
		HBox hbInVbRechts2 = new HBox(30);
		hbInVbRechts2.getChildren().addAll(txt2, timeSlider);
		VBox vbRechts = new VBox(50);
		vbRechts.getChildren().addAll(spielerTextFlow, hbInVbRechts, statusText, hbInVbRechts2);
		vbRechts.setPrefSize(550, 950);
		VBox.setMargin(spielerTextFlow, new Insets(60, 0, 20, 0));
		VBox.setMargin(hbInVbRechts2, new Insets(60, 0, 0, 0));
		BorderPane root = new BorderPane();
		try {
			this.getClass().getClassLoader();
			ImageView iv1 = new ImageView(
					new Image(this.getClass().getResourceAsStream("/m�dn/resources/wood_texture.jpg")));
			root.getChildren().add(iv1);
		}
		catch (Exception e) {
			root.setStyle("-fx-background: #8B6508;");
		}
		root.setTop(menuBar);
		root.setCenter(worldPane);
		root.setRight(vbRechts);
		this.primaryStage = primaryStage;
		primaryStage.setTitle("Mensch �rgere Dich Nicht");
		primaryStage.setScene(new Scene(root, 1500, 900));
		primaryStage.sizeToScene();
		primaryStage.setResizable(true);
		primaryStage.toFront();
	}

	public void show() {
		primaryStage.show();
	}

	@Override
	public void onChange(final Spielbrett sb) {
		if (Platform.isFxApplicationThread()) {
			worldPane.drawFigures(sb);
		}
		else {
			Platform.runLater(() -> {
				worldPane.drawFigures(sb);
			});
		}
	}

	@Override
	public void onChange(final W�rfel w) {
		if (Platform.isFxApplicationThread()) {
			w�rfelPane.drawW�rfel(w);
		}
		else {
			Platform.runLater(() -> {
				w�rfelPane.drawW�rfel(w);
			});
		}
	}

	@Override
	public void onChange(final List<M�dnSpieler> ergebnisliste) {
		if (Platform.isFxApplicationThread()) {
			GameFinishedView gfs = new GameFinishedView(ergebnisliste);
			gfs.showAndWait();
		}
		else {
			Platform.runLater(() -> {
				GameFinishedView gfs = new GameFinishedView(ergebnisliste);
				gfs.showAndWait();
			});
		}
	}

	@Override
	public void onChange(final String zugMessage) {
		if (Platform.isFxApplicationThread()) {
			statusText.appendText(zugMessage + "\n");
		}
		else {
			Platform.runLater(() -> {
				statusText.appendText(zugMessage + "\n");
			});
		}
	}

	@Override
	public void onChange(final M�dnSpieler currentSpieler) {
		if (Platform.isFxApplicationThread()) {
			Text txt = new Text(currentSpieler.getName());
			txt.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.ITALIC, 35));
			txt.setFill(Color.web(currentSpieler.getFarbe()));
			spielerTextFlow.getChildren().set(1, txt);
		}
		else {
			Platform.runLater(() -> {
				Text txt = new Text(currentSpieler.getName());
				txt.setFont(Font.font("Arial", FontWeight.BOLD, FontPosture.ITALIC, 35));
				txt.setFill(Color.web(currentSpieler.getFarbe()));
				spielerTextFlow.getChildren().set(1, txt);
			});
		}
	}

	public TextArea getStatusText() {
		return statusText;
	}

	public WorldPane getWorldPane() {
		return worldPane;
	}

	public W�rfelPane getW�rfelPane() {
		return w�rfelPane;
	}

	public MenuItem getNewGameItem() {
		return newGameItem;
	}

	public MenuItem getLoadGameItem() {
		return loadGameItem;
	}

	public MenuItem getSaveGameItem() {
		return saveGameItem;
	}

	public MenuItem getStartGameItem() {
		return startGameItem;
	}

	public MenuItem getQuitGameItem() {
		return quitGameItem;
	}

	public Button getW�rfelBtn() {
		return w�rfelBtn;
	}

	public Button getFigurBewegenBtn() {
		return figurBewegenBtn;
	}

	public Button getZugBeendenBtn() {
		return zugBeendenBtn;
	}

	public Slider getTimeSlider() {
		return timeSlider;
	}

}
