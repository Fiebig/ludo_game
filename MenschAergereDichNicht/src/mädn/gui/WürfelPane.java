package m�dn.gui;

import javafx.animation.FadeTransition;
import javafx.geometry.Pos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import m�dn.model.w�rfel.W�rfel;

public class W�rfelPane extends StackPane {

	private final double w�rfelSize;
	private final Canvas[][] canvasArrayW�rfel;
	private final GridPane w�rfelGrid;
	private final FadeTransition ft;

	public W�rfelPane(double w�rfelSize) {
		this.w�rfelSize = w�rfelSize;
		this.canvasArrayW�rfel = new Canvas[3][3];
		this.w�rfelGrid = new GridPane();
		this.ft = new FadeTransition(Duration.millis(1000), w�rfelGrid);
		this.setMinSize(w�rfelSize, w�rfelSize);
		this.setMaxSize(w�rfelSize, w�rfelSize);
		init();
	}

	private void init() {
		w�rfelGrid.setMinSize(w�rfelSize, w�rfelSize);
		w�rfelGrid.setMaxSize(w�rfelSize, w�rfelSize);
		w�rfelGrid.setHgap(15);
		w�rfelGrid.setVgap(15);
		w�rfelGrid.setAlignment(Pos.CENTER);
		// Fuer Startw�rfel
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				Canvas c = new Canvas(15, 15);
				c.getGraphicsContext2D().setFill(Color.BLACK);
				canvasArrayW�rfel[i][j] = c;
				w�rfelGrid.add(canvasArrayW�rfel[i][j], 0 + i, 0 + j, 1, 1);
			}
		}
		ft.setFromValue(0.0);
		ft.setToValue(1.0);
		ft.setCycleCount(1);
		ft.setAutoReverse(false);
		final Canvas w�rfelBoden = new Canvas(w�rfelSize, w�rfelSize);
		GraphicsContext gc = w�rfelBoden.getGraphicsContext2D();
		gc.setFill(Color.WHITE);
		gc.fillRect(0, 0, w�rfelSize, w�rfelSize);
		gc.setStroke(Color.BLACK);
		gc.setLineWidth(3);
		gc.strokeRect(0, 0, w�rfelBoden.getWidth(), w�rfelBoden.getHeight());
		this.getChildren().addAll(w�rfelBoden, w�rfelGrid);
	}

	public void drawW�rfel(W�rfel w) {
		clearWuerfel();
		int aktuelleW�rfelZahl = w.getAktuelleAugenZahl();
		switch (aktuelleW�rfelZahl) {
		case 1:
			canvasArrayW�rfel[1][1].getGraphicsContext2D().fillOval(0, 0,
					canvasArrayW�rfel[1][1].getWidth(),
					canvasArrayW�rfel[1][1].getHeight());
			break;
		case 2:
			canvasArrayW�rfel[0][0].getGraphicsContext2D().fillOval(0, 0,
					canvasArrayW�rfel[0][0].getWidth(),
					canvasArrayW�rfel[0][0].getHeight());
			canvasArrayW�rfel[2][2].getGraphicsContext2D().fillOval(0, 0,
					canvasArrayW�rfel[2][2].getWidth(),
					canvasArrayW�rfel[2][2].getHeight());
			break;
		case 3:
			canvasArrayW�rfel[0][0].getGraphicsContext2D().fillOval(0, 0,
					canvasArrayW�rfel[0][0].getWidth(),
					canvasArrayW�rfel[0][0].getHeight());
			canvasArrayW�rfel[1][1].getGraphicsContext2D().fillOval(0, 0,
					canvasArrayW�rfel[1][1].getWidth(),
					canvasArrayW�rfel[1][1].getHeight());
			canvasArrayW�rfel[2][2].getGraphicsContext2D().fillOval(0, 0,
					canvasArrayW�rfel[2][2].getWidth(),
					canvasArrayW�rfel[2][2].getHeight());
			break;
		case 4:
			canvasArrayW�rfel[0][0].getGraphicsContext2D().fillOval(0, 0,
					canvasArrayW�rfel[0][0].getWidth(),
					canvasArrayW�rfel[0][0].getHeight());
			canvasArrayW�rfel[0][2].getGraphicsContext2D().fillOval(0, 0,
					canvasArrayW�rfel[0][2].getWidth(),
					canvasArrayW�rfel[0][2].getHeight());
			canvasArrayW�rfel[2][0].getGraphicsContext2D().fillOval(0, 0,
					canvasArrayW�rfel[2][0].getWidth(),
					canvasArrayW�rfel[2][0].getHeight());
			canvasArrayW�rfel[2][2].getGraphicsContext2D().fillOval(0, 0,
					canvasArrayW�rfel[2][2].getWidth(),
					canvasArrayW�rfel[2][2].getHeight());
			break;
		case 5:
			canvasArrayW�rfel[0][0].getGraphicsContext2D().fillOval(0, 0,
					canvasArrayW�rfel[0][0].getWidth(),
					canvasArrayW�rfel[0][0].getHeight());
			canvasArrayW�rfel[0][2].getGraphicsContext2D().fillOval(0, 0,
					canvasArrayW�rfel[0][2].getWidth(),
					canvasArrayW�rfel[0][2].getHeight());
			canvasArrayW�rfel[2][0].getGraphicsContext2D().fillOval(0, 0,
					canvasArrayW�rfel[2][0].getWidth(),
					canvasArrayW�rfel[2][0].getHeight());
			canvasArrayW�rfel[2][2].getGraphicsContext2D().fillOval(0, 0,
					canvasArrayW�rfel[2][2].getWidth(),
					canvasArrayW�rfel[2][2].getHeight());
			canvasArrayW�rfel[1][1].getGraphicsContext2D().fillOval(0, 0,
					canvasArrayW�rfel[1][1].getWidth(),
					canvasArrayW�rfel[1][1].getHeight());
			break;
		case 6:
			canvasArrayW�rfel[0][0].getGraphicsContext2D().fillOval(0, 0,
					canvasArrayW�rfel[0][0].getWidth(),
					canvasArrayW�rfel[0][0].getHeight());
			canvasArrayW�rfel[0][2].getGraphicsContext2D().fillOval(0, 0,
					canvasArrayW�rfel[0][2].getWidth(),
					canvasArrayW�rfel[0][2].getHeight());
			canvasArrayW�rfel[2][0].getGraphicsContext2D().fillOval(0, 0,
					canvasArrayW�rfel[2][0].getWidth(),
					canvasArrayW�rfel[2][0].getHeight());
			canvasArrayW�rfel[2][2].getGraphicsContext2D().fillOval(0, 0,
					canvasArrayW�rfel[2][2].getWidth(),
					canvasArrayW�rfel[2][2].getHeight());
			canvasArrayW�rfel[0][1].getGraphicsContext2D().fillOval(0, 0,
					canvasArrayW�rfel[0][1].getWidth(),
					canvasArrayW�rfel[0][1].getHeight());
			canvasArrayW�rfel[2][1].getGraphicsContext2D().fillOval(0, 0,
					canvasArrayW�rfel[2][1].getWidth(),
					canvasArrayW�rfel[2][1].getHeight());
			break;
		}

		ft.play();
	}

	private void clearWuerfel() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				canvasArrayW�rfel[i][j].getGraphicsContext2D().clearRect(0, 0,
						canvasArrayW�rfel[i][j].getWidth(),
						canvasArrayW�rfel[i][j].getHeight());
			}
		}
	}

}
