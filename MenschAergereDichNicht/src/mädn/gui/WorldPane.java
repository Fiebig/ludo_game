package m�dn.gui;

import java.util.HashMap;
import java.util.List;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import m�dn.model.spielbrett.Feld;
import m�dn.model.spielbrett.Feldtyp;
import m�dn.model.spielbrett.Spielbrett;
import m�dn.model.spieler.M�dnSpieler;
import m�dn.model.spieler.SpielFigur;

public class WorldPane extends StackPane {

	private final double gridSize;
	private final GridPane worldGrid;
	private final Canvas boden;
	private final HashMap<Feld, Canvas> canvasMapWorldLayout;
	private final HashMap<Feld, Canvas> canvasMapWorldFigures;

	public WorldPane(double gridSize, Spielbrett initSb) {
		this.gridSize = gridSize;
		this.canvasMapWorldFigures = new HashMap<>();
		this.canvasMapWorldLayout = new HashMap<>();
		this.boden = new Canvas(gridSize, gridSize);
		this.worldGrid = new GridPane();
		this.setMinSize(gridSize, gridSize);
		this.setMaxSize(gridSize, gridSize);
		this.getChildren().addAll(boden, worldGrid);
		GraphicsContext gc = boden.getGraphicsContext2D();
		gc.setFill(Color.web("#F0E68C"));
		gc.fillRect(0, 0, gridSize, gridSize);
		gc.setStroke(Color.BLACK);
		gc.setLineWidth(5);
		gc.strokeRect(0, 0, gridSize, gridSize);
		initWorldGrid(initSb);
		drawWorldLayout(initSb);
	}

	public void initForGame(Spielbrett currentSb, List<M�dnSpieler> spieler) {
		canvasMapWorldFigures.clear();
		canvasMapWorldLayout.clear();
		worldGrid.getChildren().clear();
		initWorldGrid(currentSb);
		drawWorldLayout(currentSb);
		markSpielerFields(currentSb, spieler);
		drawFigures(currentSb);
	}

	private void initWorldGrid(Spielbrett currentSb) {
		worldGrid.setAlignment(Pos.CENTER);
		worldGrid.setHgap(computeGapSize(currentSb));
		worldGrid.setVgap(computeGapSize(currentSb));
		// Fuer Startwelt
		double squareSizeLayout = computeSquareSize(currentSb);
		double squareSizeFigures = (computeSquareSize(currentSb) / 100) * 70;
		for (Feld feld : currentSb.getAllFields()) {
			int xK = feld.getxK();
			int yK = feld.getyK();
			// Layout
			Canvas c = new Canvas(squareSizeLayout, squareSizeLayout);
			c.getGraphicsContext2D().setStroke(Color.BLACK);
			c.getGraphicsContext2D().setLineWidth(3);
			canvasMapWorldLayout.put(feld, c);
			worldGrid.add(c, xK, yK, 1, 1);
			// Figures
			Canvas c2 = new Canvas(squareSizeFigures, squareSizeFigures);
			c2.setTranslateX((squareSizeLayout - squareSizeFigures) / 2);
			c2.getGraphicsContext2D().setStroke(Color.BLACK);
			c2.getGraphicsContext2D().setLineWidth(2);
			canvasMapWorldFigures.put(feld, c2);
			worldGrid.add(c2, xK, yK, 1, 1);
		}
	}

	/**
	 * Zeichnet die Figuren auf die Canvas.
	 */
	public void drawFigures(Spielbrett currentSb) {
		for (final Feld feld : currentSb.getAllFields()) {
			SpielFigur sf = feld.getBesetzung();
			Canvas canvas = canvasMapWorldFigures.get(feld);
			GraphicsContext graphicContext = canvas.getGraphicsContext2D();
			if (sf != null) {
				Color color = Color.web(sf.getRelatedPlayer().getFarbe());
				graphicContext.setFill(color);
				Lighting l = new Lighting();
				l.setSurfaceScale(5);
				l.setLight(new Light.Point(40, 50, 60, Color.WHITE));
				canvas.setEffect(l);
				graphicContext.fillOval(0, 0, canvas.getWidth(), canvas.getHeight());
				graphicContext.strokeOval(0, 0, canvas.getWidth(), canvas.getHeight());

			}
			else {
				graphicContext.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
			}
		}
	}

	private void drawWorldLayout(Spielbrett currentSb) {
		for (final Feld temp : currentSb.getAllFields()) {
			Canvas canvas = canvasMapWorldLayout.get(temp);
			GraphicsContext gc = canvas.getGraphicsContext2D();

			if (temp.getFeldtyp() == Feldtyp.ZIELFELD) {
				gc.setFill(Color.gray(0.4));
			}
			else if (temp.getFeldtyp() == Feldtyp.STARTFELD) {
				gc.setFill(Color.gray(0.4));
			}
			else {
				gc.setFill(Color.rgb(205, 205, 193));
			}
			gc.fillOval(0, 0, canvas.getWidth(), canvas.getHeight());
			gc.strokeOval(0, 0, canvas.getWidth(), canvas.getHeight());
		}
	}

	private void markSpielerFields(Spielbrett currentSb, List<M�dnSpieler> spieler) {
		for (M�dnSpieler s : spieler) {
			Color color = Color.web(s.getFarbe());
			for (Feld feld : s.getSp().getFelder()) {
				markField(feld, color);
			}
			for (Feld feld : s.getZp().getFelder()) {
				markField(feld, color);
			}
			markField(currentSb.getEntryFieldForSp(s.getSp()), color);
		}
	}

	private void markField(Feld feld, Color color) {
		Canvas canvas = canvasMapWorldLayout.get(feld);
		GraphicsContext gc = canvas.getGraphicsContext2D();
		gc.setFill(color);
		gc.fillOval(0, 0, canvas.getWidth(), canvas.getHeight());
		gc.strokeOval(0, 0, canvas.getWidth(), canvas.getHeight());
	}

	/**
	 * Setzt die Maus-Events auf die Canvas um eine Figur ausw�hlen zu k�nnen.
	 * 
	 * @param w
	 *            Aktuelle Welt
	 */
	public void setFieldAction(Feld feld, EventHandler<MouseEvent> evH) {
		canvasMapWorldFigures.get(feld).addEventHandler(MouseEvent.MOUSE_CLICKED, evH);
	}

	public void clearSelectionEffect(Feld feld) {
		if (feld.getBesetzung() != null) {
			Lighting l = new Lighting();
			l.setSurfaceScale(5);
			l.setLight(new Light.Point(40, 50, 60, Color.WHITE));
			canvasMapWorldFigures.get(feld).setEffect(l);
		}
	}

	public void setSelectionEffect(Feld feld) {
		if (feld.getBesetzung() != null) {
			Lighting l = new Lighting();
			l.setSurfaceScale(5);
			l.setLight(new Light.Point(40, 50, 60, Color.WHITE));
			DropShadow ds = new DropShadow(25, Color.BLACK);
			l.setContentInput(ds);
			canvasMapWorldFigures.get(feld).setEffect(l);
		}
	}

	private double computeSquareSize(Spielbrett currentSb) {
		double verf�gbarerPlatz = (gridSize / 100) * 70;// 70%
		return verf�gbarerPlatz / Math.max(currentSb.getWidth(), currentSb.getHeight());

	}

	private double computeGapSize(Spielbrett currentSb) {
		double verf�gbarerPlatz = (gridSize / 100) * 30;// 30%
		return verf�gbarerPlatz / Math.max(currentSb.getWidth(), currentSb.getHeight());

	}
}
