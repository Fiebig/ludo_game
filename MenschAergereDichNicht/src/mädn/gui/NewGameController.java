package m�dn.gui;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Toggle;
import javafx.scene.paint.Color;
import m�dn.ai.StandardAi;
import m�dn.ai.StandardZugBewertung;
import m�dn.exceptions.IllegalGameStateException;
import m�dn.gui.SpielerAuswahl.CbItems;
import m�dn.logic.GameLogic;
import m�dn.logic.StandardGameLogic;
import m�dn.model.spielbrett.Spielbrett;
import m�dn.model.spielbrett.Startposition;
import m�dn.operationsContainer.OperationsContainer;
import m�dn.rules.StandardRules;

public class NewGameController {

	private IllegalGameStateException initError;
	// Model
	private GameLogic gl;
	// View
	private final NewGameView view;

	public NewGameController(NewGameView view) {
		this.view = view;
		view.getRbGroup().selectedToggleProperty()
				.addListener(new RbChangeListener());
		view.getBtn().setOnAction(new SpielErstellenBtnEventHandler());
	}

	public void showAndWaitNewGameView() {
		view.showAndWait();
	}

	class RbChangeListener implements ChangeListener<Toggle> {
		@Override
		public void changed(ObservableValue<? extends Toggle> ov,
				Toggle old_toggle, Toggle new_toggle) {
			if (old_toggle != null) {
				view.setSpielerAuswahl(new SpielerAuswahl(view
						.getSelectedSpielbrett().getSps().size()));
			}
		}
	}

	class SpielErstellenBtnEventHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			try {
				Spielbrett sb = view.getSelectedSpielbrett();
				gl = new StandardGameLogic(new StandardRules(), sb);
				for (int i = 0; i < view.getSpielerAuswahl().getRowCount(); i++) {
					CbItems temp = view.getSpielerAuswahl()
							.getComboBoxSelection(i);
					if (temp != CbItems.INAKTIV) {
						Startposition sp = sb.getSps().get(i);
						Color color = view.getSpielerAuswahl()
								.getColorPickerSelection(i);
						String name = view.getSpielerAuswahl()
								.getTextFieldContent(i);
						if (temp == CbItems.MENSCHLICHER_SPIELER) {
							gl.addMenschlichenSpieler(
									name,
									OperationsContainer.colorToHexString(color),
									sp);
						}
						else {
							gl.addAiSpieler(
									name,
									OperationsContainer.colorToHexString(color),
									sp, new StandardZugBewertung(),
									new StandardAi());
						}
					}
				}
			}
			catch (IllegalGameStateException e) {
				initError = e;
			}
			finally {
				view.close();
			}
		}
	}

	public GameLogic getGl() throws IllegalGameStateException {
		if (initError != null) {
			throw initError;
		}
		return gl;
	}

}
