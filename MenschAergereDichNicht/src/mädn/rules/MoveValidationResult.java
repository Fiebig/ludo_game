package m�dn.rules;

import m�dn.exceptions.M�dnException;
import m�dn.model.spielbrett.Feld;
import m�dn.model.spieler.SpielerIngameStatus;

public class MoveValidationResult {

	private final Feld targetField;
	private final SpielerIngameStatus nextPlayerState;
	private final boolean isValid;
	private final M�dnException error;

	public MoveValidationResult(Feld targetField,
			SpielerIngameStatus nextPlayerState) {
		this.targetField = targetField;
		this.nextPlayerState = nextPlayerState;
		this.isValid = true;
		this.error = null;
	}

	public MoveValidationResult(M�dnException error) {
		this.targetField = null;
		this.nextPlayerState = null;
		this.isValid = false;
		this.error = error;
	}

	public Feld getTargetField() {
		return targetField;
	}

	public SpielerIngameStatus getNextPlayerState() {
		return nextPlayerState;
	}

	public boolean isValid() {
		return isValid;
	}

	public M�dnException getError() {
		return error;
	}

}
