package m�dn.rules;

import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

import m�dn.exceptions.IllegalMoveException;
import m�dn.exceptions.IllegalPlayerStateException;
import m�dn.logic.GameLogic;
import m�dn.model.spielbrett.Feld;
import m�dn.model.spielbrett.Feldtyp;
import m�dn.model.spielbrett.Spielbrett;
import m�dn.model.spieler.M�dnSpieler;
import m�dn.model.spieler.SpielFigur;
import m�dn.model.spieler.SpielerIngameStatus;

public class StandardRules implements Rules {

	private static final long serialVersionUID = 1L;

	@Override
	public boolean gameFinished(GameLogic gl) {
		int temp = 0;
		for (ListIterator<M�dnSpieler> i = gl.getPlayers().listIterator(); i
				.hasNext();) {
			if (!isFinished(i.next()) && ++temp > 1) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean isFinished(M�dnSpieler spieler) {
		return spieler.getZp().isFull();
	}

	@Override
	public boolean kannZugBeenden(M�dnSpieler spieler) {
		if (spieler.getStatus() == SpielerIngameStatus.WARTET_AUF_N�CHSTEN_ZUG) {
			return true;
		}
		int wuerfelZahl = spieler.getGl().getW�rfel().getAktuelleAugenZahl();
		return spieler.getStatus() == SpielerIngameStatus.ZIEHPHASE
				&& wuerfelZahl != 6
				&& getMoveOptions(spieler, wuerfelZahl) == null;
	}

	@Override
	public MoveValidationResult moveValidation(M�dnSpieler spieler,
			SpielFigur ausgew�hlteFigur) {
		try {
			spielerStateCheck(spieler);
			figurCheck(spieler, ausgew�hlteFigur);
			int wuerfelZahl = spieler.getGl().getW�rfel()
					.getAktuelleAugenZahl();
			Feld targetFeld = getTargetFeld(wuerfelZahl, ausgew�hlteFigur);
			targetFeldCheck(spieler, targetFeld);
			// IngameStatus setten
			if (wuerfelZahl == 6) {
				return new MoveValidationResult(targetFeld,
						SpielerIngameStatus.W�RFELPHASE);
			}
			else {
				return new MoveValidationResult(targetFeld,
						SpielerIngameStatus.WARTET_AUF_N�CHSTEN_ZUG);
			}
		}
		catch (IllegalPlayerStateException | IllegalMoveException e) {
			return new MoveValidationResult(e);
		}
	}

	@Override
	public boolean kannWuerfeln(M�dnSpieler spieler) {
		/*
		 * Erlaubt direktes weiteres W�rfeln falls eine 6 geworfen wurde und
		 * kein Zug m�glich ist
		 */
		return (spieler.getStatus() == SpielerIngameStatus.ZIEHPHASE
				&& spieler.getGl().getW�rfel().getAktuelleAugenZahl() == 6 && getMoveOptions(
				spieler, 6) == null)
				|| (spieler.getStatus() == SpielerIngameStatus.W�RFELPHASE);
	}

	@Override
	public HashMap<SpielFigur, Feld> getMoveOptions(M�dnSpieler spieler,
			int wuerfelZahl) {

		if (wuerfelZahl < 1 || wuerfelZahl > 6) {
			throw new IllegalArgumentException(
					"Eine WuerfelZahl muss zwischen 1 und 6 liegen!");
		}
		final HashMap<SpielFigur, Feld> zuordnungen = new HashMap<SpielFigur, Feld>();
		for (ListIterator<SpielFigur> i = spieler.getStrategies()
				.listIterator(); i.hasNext();) {
			try {
				SpielFigur figurTemp = i.next();
				Feld feldTemp = getTargetFeld(wuerfelZahl, figurTemp);
				targetFeldCheck(spieler, feldTemp);
				zuordnungen.put(figurTemp, feldTemp);
			}
			catch (IllegalMoveException e) {
			}
		}

		if (zuordnungen.isEmpty()) {
			return null;
		}
		return zuordnungen;
	}

	private Feld getTargetFeld(int wuerfelZahl, SpielFigur sf)
			throws IllegalMoveException {

		final M�dnSpieler spieler = sf.getRelatedPlayer();
		final Spielbrett spielbrett = spieler.getGl().getSpielbrett();
		final List<Feld> fieldList = spielbrett.getFieldsInOrderForZp(spieler
				.getZp());

		if (sf.getCurrentField().getFeldtyp() == Feldtyp.STARTFELD) {
			if (wuerfelZahl == 6) {
				return fieldList.get(0);
			}
			throw new IllegalMoveException(
					"Es muss eine 6 gew�rfelt worden sein um eine Spielfigur aus einem Startfeld zu bewegen!");
		}
		else if (sf.getCurrentField().getFeldtyp() == Feldtyp.STANDARDFELD) {
			int index = fieldList.indexOf(sf.getCurrentField()) + wuerfelZahl;
			if (index < fieldList.size()) {
				return fieldList.get(index);
			}
			throw new IllegalMoveException(
					"Spielfigur kann mit einer "
							+ wuerfelZahl
							+ " aus aktueller Position nicht auf ein Zielfeld bewegt werden!");
		}
		else {
			int index = fieldList.indexOf(sf.getCurrentField()) + wuerfelZahl;
			if (index < fieldList.size()) {
				return fieldList.get(index);
			}
			throw new IllegalMoveException(
					"Spielfigur im Zielfeld kann nicht mehr um " + wuerfelZahl
							+ " bewegt werden!");
		}
	}

	private void figurCheck(M�dnSpieler spieler, SpielFigur ausgew�hlteFigur)
			throws IllegalMoveException {
		if (ausgew�hlteFigur == null) {
			throw new IllegalMoveException(spieler.getName()
					+ " hat noch keine Figur ausgew�hlt!");
		}

		if (ausgew�hlteFigur.getRelatedPlayer().equals(spieler) == false) {
			throw new IllegalMoveException(
					"Spieler darf nur seine eigenen Spielfiguren bewegen!");
		}
	}

	private void spielerStateCheck(M�dnSpieler spieler)
			throws IllegalPlayerStateException {
		if (spieler.getStatus() == SpielerIngameStatus.W�RFELPHASE) {
			throw new IllegalPlayerStateException(spieler.getName()
					+ " muss in diesem Zug noch w�rfeln!");
		}

		if (spieler.getStatus() == SpielerIngameStatus.WARTET_AUF_N�CHSTEN_ZUG) {
			throw new IllegalPlayerStateException(spieler.getName()
					+ " darf in diesem Zug keine Figur mehr bewegen!");
		}

	}

	private void targetFeldCheck(M�dnSpieler spieler, Feld targetfeld)
			throws IllegalMoveException {

		if (targetfeld.getFeldtyp() == Feldtyp.STARTFELD) {
			throw new IllegalMoveException(
					"Spieler darf Spielfigur nicht auf ein Startfeld bewegen!");
		}
		if (targetfeld.getFeldtyp() == Feldtyp.ZIELFELD
				&& spieler.getZp().getFelder().contains(targetfeld) == false) {
			throw new IllegalMoveException(
					"Spieler darf Spielfigur nur auf seine Zielfelder bewegen!");
		}
		if (targetfeld.getBesetzung() != null
				&& targetfeld.getBesetzung().getRelatedPlayer().equals(spieler)) {
			throw new IllegalMoveException(
					"Spieler kann seine eigenen Spielfiguren nicht schmeissen");
		}

	}

}
