package m�dn.rules;

import java.io.Serializable;
import java.util.HashMap;

import m�dn.exceptions.IllegalMoveException;
import m�dn.exceptions.IllegalPlayerStateException;
import m�dn.logic.GameLogic;
import m�dn.model.spielbrett.Feld;
import m�dn.model.spieler.M�dnSpieler;
import m�dn.model.spieler.SpielFigur;

public interface Rules extends Serializable {
	/**
	 * Pr�ft, ob dass Spiel beendet ist.
	 * 
	 * @param gl
	 *            Logik des Spiels
	 * @return true, wenn Spiel zuende ist
	 */
	public boolean gameFinished(GameLogic gl);

	/**
	 * Pr�ft, ob ein Spieler fertig ist.
	 * 
	 * @param spieler
	 *            Spieler der gepr�ft werden soll
	 * @return true, wenn spieler fertig ist
	 */
	public boolean isFinished(M�dnSpieler spieler);

	/**
	 * Pr�ft ob spieler den aktuellen Zug beenden darf.
	 * 
	 * @param spieler
	 * @throws IllegalStateException
	 *             falls spieler Zug noch nicht beenden darf
	 */
	public boolean kannZugBeenden(M�dnSpieler spieler);

	/**
	 * Bewegt ausgew�hlte Figur um wuerfelZahl Felder falls m�glich.
	 * 
	 * @param spieler
	 *            Spieler der Figur bewegen soll
	 * @param ausgew�hlteFigur
	 *            Spielfigur, die spieler ausgew�hlt hat
	 * @throws IllegalPlayerStateException
	 *             TODO
	 * @throws IllegalMoveException
	 *             TODO
	 */
	public MoveValidationResult moveValidation(M�dnSpieler spieler,
			SpielFigur ausgew�hlteFigur);

	/**
	 * spieler w�rfelt. W�rfelt erh�lt neue Augenzahl und flags von spieler
	 * werden ver�ndert.
	 * 
	 * @param spieler
	 *            Spieler, der w�rfeln soll
	 * @throws IllegalStateException
	 *             falls spieler nicht mehr w�rfeln darf
	 */
	public boolean kannWuerfeln(M�dnSpieler spieler);

	/**
	 * Liefert eine Hashmap, die alle Figuren von spieler mit denen mit
	 * w�rfelzahl ein Zug m�glich ist auf das jeweilige Targetfeld mappt.
	 * 
	 * @param spieler
	 *            Spieler, f�r den die Bewegungsoptionen geliefert werden sollen
	 * @return Bewegungsoptionen als Map oder null falls es keine
	 *         Bewegungsoptionen gibt
	 */
	public HashMap<SpielFigur, Feld> getMoveOptions(M�dnSpieler spieler,
			int wuerfelZahl);

}