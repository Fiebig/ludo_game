package m�dn.exceptions;

public class IllegalGameStateException extends M�dnException {

	private static final long serialVersionUID = 1L;

	public IllegalGameStateException(String message) {
		super(message);
	}

	public IllegalGameStateException(String message, Throwable cause) {
		super(message, cause);
	}

	public IllegalGameStateException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
