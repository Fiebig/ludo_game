package m�dn.exceptions;

public class IllegalMoveException extends M�dnException {

	private static final long serialVersionUID = 1L;

	public IllegalMoveException(String message) {
		super(message);
	}

	public IllegalMoveException(String message, Throwable cause) {
		super(message, cause);
	}

	public IllegalMoveException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
