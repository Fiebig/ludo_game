package m�dn.control;

import java.io.OutputStream;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import m�dn.logic.GameChangedListener;
import m�dn.logic.ObservableGameLogicDecorator;
import m�dn.model.spielbrett.Spielbrett;
import m�dn.model.spieler.AiSpieler;
import m�dn.model.spieler.M�dnSpieler;

/**
 * @author Alex
 *
 */
public class ThreadingGameController {

	private final ObservableGameLogicDecorator ogld;
	private final AiThread aiThread;
	private final ReentrantLock lock = new ReentrantLock();
	private final ReentrantLock lock2 = new ReentrantLock();
	private final Condition n�chsterAiSpieler = lock.newCondition();
	private final Condition pauseZuende = lock.newCondition();
	private final Condition aiWaits = lock2.newCondition();

	/**
	 * Initialisiert ein Objekt mit der entsprechenden Spiellogik.
	 * 
	 * @param logic
	 *            Zugrunde liegende Spiellogik
	 */
	public ThreadingGameController(ObservableGameLogicDecorator ogld) {
		this.ogld = ogld;
		this.aiThread = new AiThread(1000);
		aiThread.setName("AiThread");
		aiThread.setDaemon(true);
	}

	public void figurBewegen() {
		ogld.figurBewegen();
	}

	public void zugBeenden() {
		ogld.zugBeenden();
		if (ogld.getSpielerAmZug() instanceof AiSpieler) {
			resumeAiThread();
		}
	}

	public M�dnSpieler getSpielerAmZug() {
		return ogld.getSpielerAmZug();
	}

	public Spielbrett getSpielbrett() {
		return ogld.getSpielbrett();
	}

	public List<M�dnSpieler> getSpielerList() {
		return ogld.getPlayers();
	}

	public boolean gameFinished() {
		return ogld.gameFinished();
	}

	public void addGameChangedListener(GameChangedListener listener) {
		ogld.addGameChangedListener(listener);
	}

	public void removeGameChangedListener(GameChangedListener listener) {
		ogld.removeGameChangedListener(listener);
	}

	public void wuerfle() {
		ogld.w�rfeln();
	}

	public boolean isGameStarted() {
		return ogld.isGameStarted();
	}

	public void saveGame(OutputStream out) {
		if (!isAiWaiting() && aiThread.isAlive()) {
			lock2.lock();
			try {
				aiWaits.await();
			}
			catch (InterruptedException e) {
			}
			finally {
				lock2.unlock();
			}
		}
		ogld.saveGame(out);
	}

	/**
	 * Pr�ft ob der AiThread pausiert.
	 */
	private boolean isAiWaiting() {
		return aiThread.getState() == Thread.State.WAITING;
	}

	private void resumeAiThread() {
		if (isAiWaiting()) {
			lock.lock();
			n�chsterAiSpieler.signal();
			lock.unlock();
		}
	}

	public void terminateAiThread() {
		aiThread.interrupt();
	}

	public void setAiTimeout(int timeOut) {
		aiThread.sleepTime = timeOut;
	}

	public void start() {
		// F�r geladenen Spielstand
		if (ogld.isGameStarted() && aiThread.getState() == Thread.State.NEW) {
			aiThread.start();
		}
		else {
			ogld.startGame();
			if (ogld.isGameStarted()) {
				aiThread.start();
			}
		}
	}

	public void setPauseAiThread(boolean shouldPause) {
		aiThread.shouldWait = shouldPause;
		if (shouldPause == false && isAiWaiting()) {
			lock.lock();
			pauseZuende.signal();
			lock.unlock();
		}
	}

	/**
	 * Innere Klasse von ThreadingGameController, die den Thread implementiert,
	 * in dem die Ai l�uft.
	 * 
	 * @author Alexander Fiebig
	 * 
	 */
	private class AiThread extends Thread {
		private int sleepTime;
		private boolean shouldWait;

		/**
		 * Erzeugt einen neuen Spielthread.
		 * 
		 * @param sleepTime
		 *            Wartezeit zwischen den einzelnen Iterationen
		 */
		private AiThread(int sleepTime) {
			this.sleepTime = sleepTime;
			this.shouldWait = false;
		}

		@Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()
					&& !ThreadingGameController.this.gameFinished()) {
				try {
					lock.lockInterruptibly();
					try {
						if (getSpielerAmZug() instanceof AiSpieler == false) {
							n�chsterAiSpieler.await();
						}
						if (shouldWait == true) {
							lock2.lock();
							aiWaits.signal();
							lock2.unlock();
							pauseZuende.await();
						}
						Thread.sleep(sleepTime);
						wuerfle();
						Thread.sleep(sleepTime);
						figurBewegen();
					}
					finally {
						if (ogld.getRules().kannZugBeenden(
								ogld.getSpielerAmZug())) {
							zugBeenden();
						}
						lock.unlock();
					}
				}
				catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
			}
		}
	}
}
