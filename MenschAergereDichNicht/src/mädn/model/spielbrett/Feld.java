package m�dn.model.spielbrett;

import java.io.Serializable;

import m�dn.model.spieler.SpielFigur;

public class Feld implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final Feldtyp feldtyp;
	private SpielFigur besetzung;
	private final int xK;
	private final int yK;

	protected Feld(int xK, int yK, Feldtyp feldtyp) {
		this.xK = xK;
		this.yK = yK;
		this.feldtyp = feldtyp;
	}

	public int getxK() {
		return xK;
	}
	public int getyK() {
		return yK;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + xK;
		result = prime * result + yK;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Feld other = (Feld) obj;
		if (xK != other.xK) {
			return false;
		}
		if (yK != other.yK) {
			return false;
		}
		return true;
	}

	public SpielFigur getBesetzung() {
		return besetzung;
	}

	public void setBesetzung(SpielFigur besetzung) {
		this.besetzung = besetzung;
	}

	public Feldtyp getFeldtyp() {
		return feldtyp;
	}

	@Override
	public String toString() {
		return "Feld;;;" + xK + "," + yK + System.getProperty("line.separator");
	}

}