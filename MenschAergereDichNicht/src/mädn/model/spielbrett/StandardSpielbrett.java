package m�dn.model.spielbrett;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StandardSpielbrett implements Spielbrett {

	private static final long serialVersionUID = 1L;
	private final Map<Zielposition, List<Feld>> wayMap;
	private final List<Feld> stdFelder;
	private final List<Feld> allFields;
	private final List<Startposition> sps;
	private final List<Zielposition> zps;

	public StandardSpielbrett() {
		this.stdFelder = new ArrayList<>(40);
		this.sps = new ArrayList<>(4);
		this.zps = new ArrayList<>(4);
		this.wayMap = new HashMap<>(4);
		createStdFields();
		createStartpositionen();
		createZielpositionen();
		createWays();
		this.allFields = new ArrayList<Feld>(stdFelder);
		for (Startposition sp : sps) {
			allFields.addAll(sp.getFelder());
			allFields.addAll(getZpForSp(sp).getFelder());
		}
	}

	private void createStdFields() {
		// nach Eingang1
		stdFelder.add(new Feld(6, 0, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(6, 1, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(6, 2, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(6, 3, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(6, 4, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(7, 4, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(8, 4, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(9, 4, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(10, 4, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(10, 5, Feldtyp.STANDARDFELD));
		// nach Eingang2
		stdFelder.add(new Feld(10, 6, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(9, 6, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(8, 6, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(7, 6, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(6, 6, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(6, 7, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(6, 8, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(6, 9, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(6, 10, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(5, 10, Feldtyp.STANDARDFELD));
		// nach Eingang3
		stdFelder.add(new Feld(4, 10, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(4, 9, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(4, 8, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(4, 7, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(4, 6, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(3, 6, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(2, 6, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(1, 6, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(0, 6, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(0, 5, Feldtyp.STANDARDFELD));
		// nach Eingang4
		stdFelder.add(new Feld(0, 4, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(1, 4, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(2, 4, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(3, 4, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(4, 4, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(4, 3, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(4, 2, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(4, 1, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(4, 0, Feldtyp.STANDARDFELD));
		stdFelder.add(new Feld(5, 0, Feldtyp.STANDARDFELD));
	}

	private void createStartpositionen() {
		sps.add(new Startposition(new Feld(0, 0, Feldtyp.STARTFELD), new Feld(
				0, 1, Feldtyp.STARTFELD), new Feld(1, 0, Feldtyp.STARTFELD),
				new Feld(1, 1, Feldtyp.STARTFELD)));
		sps.add(new Startposition(new Feld(9, 0, Feldtyp.STARTFELD), new Feld(
				9, 1, Feldtyp.STARTFELD), new Feld(10, 0, Feldtyp.STARTFELD),
				new Feld(10, 1, Feldtyp.STARTFELD)));
		sps.add(new Startposition(new Feld(10, 10, Feldtyp.STARTFELD),
				new Feld(9, 9, Feldtyp.STARTFELD), new Feld(10, 9,
						Feldtyp.STARTFELD), new Feld(9, 10, Feldtyp.STARTFELD)));
		sps.add(new Startposition(new Feld(0, 9, Feldtyp.STARTFELD), new Feld(
				0, 10, Feldtyp.STARTFELD), new Feld(1, 9, Feldtyp.STARTFELD),
				new Feld(1, 10, Feldtyp.STARTFELD)));

	}

	private void createZielpositionen() {
		zps.add(new Zielposition(new Feld(1, 5, Feldtyp.ZIELFELD), new Feld(2,
				5, Feldtyp.ZIELFELD), new Feld(3, 5, Feldtyp.ZIELFELD),
				new Feld(4, 5, Feldtyp.ZIELFELD)));
		zps.add(new Zielposition(new Feld(5, 1, Feldtyp.ZIELFELD), new Feld(5,
				2, Feldtyp.ZIELFELD), new Feld(5, 3, Feldtyp.ZIELFELD),
				new Feld(5, 4, Feldtyp.ZIELFELD)));
		zps.add(new Zielposition(new Feld(9, 5, Feldtyp.ZIELFELD), new Feld(8,
				5, Feldtyp.ZIELFELD), new Feld(7, 5, Feldtyp.ZIELFELD),
				new Feld(6, 5, Feldtyp.ZIELFELD)));
		zps.add(new Zielposition(new Feld(5, 9, Feldtyp.ZIELFELD), new Feld(5,
				8, Feldtyp.ZIELFELD), new Feld(5, 7, Feldtyp.ZIELFELD),
				new Feld(5, 6, Feldtyp.ZIELFELD)));

	}

	private void createWays() {
		List<Feld> zp0 = new ArrayList<>(
				stdFelder.subList(30, stdFelder.size()));
		zp0.addAll(stdFelder.subList(0, 30));
		zp0.addAll(zps.get(0).getFelder());
		//
		List<Feld> zp1 = new ArrayList<>(stdFelder);
		zp1.addAll(zps.get(1).getFelder());
		//
		List<Feld> zp2 = new ArrayList<>(
				stdFelder.subList(10, stdFelder.size()));
		zp2.addAll(stdFelder.subList(0, 10));
		zp2.addAll(zps.get(2).getFelder());
		//
		List<Feld> zp3 = new ArrayList<>(
				stdFelder.subList(20, stdFelder.size()));
		zp3.addAll(stdFelder.subList(0, 20));
		zp3.addAll(zps.get(3).getFelder());

		wayMap.put(zps.get(0), zp0);
		wayMap.put(zps.get(1), zp1);
		wayMap.put(zps.get(2), zp2);
		wayMap.put(zps.get(3), zp3);
	}

	@Override
	public List<Feld> getAllFields() {
		return Collections.unmodifiableList(allFields);
	}

	@Override
	public List<Startposition> getSps() {
		return Collections.unmodifiableList(sps);
	}

	@Override
	public List<Feld> getFieldsInOrderForZp(Zielposition zp) {
		return wayMap.get(zp);
	}

	@Override
	public List<Feld> getStandardFelderInOrder() {
		return Collections.unmodifiableList(stdFelder);
	}

	@Override
	public Feld getEntryFieldForSp(Startposition sp) {

		if (sp.equals(sps.get(0))) {
			return stdFelder.get(30);
		}
		else if (sp.equals(sps.get(1))) {
			return stdFelder.get(0);
		}
		else if (sp.equals(sps.get(2))) {
			return stdFelder.get(10);
		}
		else {
			return stdFelder.get(20);
		}
	}

	@Override
	public Zielposition getZpForSp(Startposition sp) {

		if (sp.equals(sps.get(0))) {
			return zps.get(0);
		}
		else if (sp.equals(sps.get(1))) {
			return zps.get(1);
		}
		else if (sp.equals(sps.get(2))) {
			return zps.get(2);
		}
		else {
			return zps.get(3);
		}

	}

	@Override
	public int getWidth() {
		return 11;
	}

	@Override
	public int getHeight() {
		return 11;
	}

}
