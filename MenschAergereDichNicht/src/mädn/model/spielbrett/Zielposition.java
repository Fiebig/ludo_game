package m�dn.model.spielbrett;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

public class Zielposition implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private final List<Feld> felder;

	protected Zielposition(Feld... felder) {
		if (felder.length == 0) {
			throw new IllegalArgumentException(
					"Eine Zielposition muss aus mindestens einem Feld bestehen!");
		}

		for (int i = 0; i < felder.length; i++) {
			if (felder[i].getFeldtyp() != Feldtyp.ZIELFELD) {
				throw new IllegalArgumentException(
						"Eine Zielposition muss aus Zielfeldern bestehen!");
			}
		}
		this.felder = Arrays.asList(felder);
	}

	public List<Feld> getFelder() {
		return Collections.unmodifiableList(felder);
	}

	public int anzahlFelder() {
		return felder.size();
	}

	public boolean isEmpty() {

		for (ListIterator<Feld> l = felder.listIterator(); l.hasNext();) {
			Feld feld = l.next();
			if (feld.getBesetzung() != null) {
				return false;
			}
		}
		return true;
	}

	public boolean isFull() {

		for (ListIterator<Feld> l = felder.listIterator(); l.hasNext();) {
			Feld feld = l.next();
			if (feld.getBesetzung() == null) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((felder == null) ? 0 : felder.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Zielposition other = (Zielposition) obj;
		if (felder == null) {
			if (other.felder != null) {
				return false;
			}
		} else if (!felder.equals(other.felder)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Zielposition;;;"
				+ System.getProperty("line.separator"));
		for (ListIterator<Feld> l = felder.listIterator(); l.hasNext();) {
			sb.append(l.next().toString());

		}
		return sb.toString();
	}
}