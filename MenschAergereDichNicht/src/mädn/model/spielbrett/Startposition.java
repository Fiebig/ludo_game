package m�dn.model.spielbrett;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

public class Startposition implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private final List<Feld> felder;

	protected Startposition(Feld... felder) {
		if (felder.length == 0) {
			throw new IllegalArgumentException(
					"Eine Startposition muss aus mindestens einem Feld bestehen!");
		}
		for (int i = 0; i < felder.length; i++) {
			if (felder[i].getFeldtyp() != Feldtyp.STARTFELD) {
				throw new IllegalArgumentException(
						"Eine Startposition muss aus Startfeldern bestehen!");
			}
		}
		this.felder = Arrays.asList(felder);
	}

	public List<Feld> getFelder() {
		return Collections.unmodifiableList(felder);

	}

	public int anzahlFelder() {
		return felder.size();
	}

	public boolean isEmpty() {

		for (ListIterator<Feld> l = felder.listIterator(); l.hasNext();) {
			Feld feld = l.next();
			if (feld.getBesetzung() != null) {
				return false;
			}
		}
		return true;
	}

	public boolean isFull() {

		for (ListIterator<Feld> l = felder.listIterator(); l.hasNext();) {
			Feld feld = l.next();
			if (feld.getBesetzung() == null) {
				return false;
			}
		}
		return true;
	}

	public Feld getFreiesStartfeld() {
		for (ListIterator<Feld> l = felder.listIterator(); l.hasNext();) {
			Feld feld = l.next();
			if (feld.getBesetzung() == null) {
				return feld;
			}
		}
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((felder == null) ? 0 : felder.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Startposition other = (Startposition) obj;
		if (felder == null) {
			if (other.felder != null) {
				return false;
			}
		} else if (!felder.equals(other.felder)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Startposition;;;"
				+ System.getProperty("line.separator"));
		for (ListIterator<Feld> l = felder.listIterator(); l.hasNext();) {
			sb.append(l.next().toString());

		}
		return sb.toString();
	}

}