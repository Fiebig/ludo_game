package m�dn.model.spielbrett;

import java.io.Serializable;
import java.util.List;

public interface Spielbrett extends Serializable {

	public List<Feld> getAllFields();

	public List<Startposition> getSps();

	public List<Feld> getFieldsInOrderForZp(Zielposition zp);

	public List<Feld> getStandardFelderInOrder();

	public Feld getEntryFieldForSp(Startposition sp);

	public Zielposition getZpForSp(Startposition sp);

	public int getWidth();

	public int getHeight();

}
