package m�dn.model.spieler;

import gametheory.FiniteProfitPlayer;

import java.util.ListIterator;

import m�dn.exceptions.IllegalMoveException;
import m�dn.exceptions.IllegalPlayerStateException;
import m�dn.exceptions.M�dnException;
import m�dn.logic.GameLogic;
import m�dn.model.spielbrett.Feld;
import m�dn.model.spielbrett.Startposition;
import m�dn.model.spielbrett.Zielposition;
import m�dn.rules.MoveValidationResult;

public abstract class M�dnSpieler extends FiniteProfitPlayer<M�dnSpieler, SpielFigur> {

	private static final long serialVersionUID = 1L;
	protected final Zielposition zp;
	protected final Startposition sp;
	protected final String farbe;
	protected SpielerIngameStatus status;
	protected final GameLogic gl;

	public M�dnSpieler(String name, String farbe, Startposition sp, GameLogic gl) {
		super(name);
		if (gl.getSpielbrett().getSps().contains(sp) == false) {
			throw new IllegalArgumentException(
					"sp geh�rt nicht zu spielbrett von gl!");
		}
		this.farbe = farbe;
		this.sp = sp;
		this.gl = gl;
		this.zp = gl.getSpielbrett().getZpForSp(sp);
		this.status = SpielerIngameStatus.WARTET_AUF_N�CHSTEN_ZUG;
		for (Feld currentField : sp.getFelder()) {
			SpielFigur sf = new SpielFigur(this, currentField);
			currentField.setBesetzung(sf);
			strategies.add(sf);
		}
		this.currentStrategy = null;
	}

	public final void wuerfle() throws IllegalPlayerStateException {
		if (!gl.getRules().kannWuerfeln(this)) {
			throw new IllegalPlayerStateException(this.name
					+ " darf in diesem Zug nicht mehr w�rfeln!");
		}
		gl.getW�rfel().neueAugenZahl();
		status = SpielerIngameStatus.ZIEHPHASE;
	}

	public final void zugBeenden() throws IllegalPlayerStateException {
		if (!gl.getRules().kannZugBeenden(this)) {
			throw new IllegalPlayerStateException(
					this.name
							+ " muss in diesem Zug noch w�rfeln und/oder eine Figur bewegen!");

		}
		status = SpielerIngameStatus.WARTET_AUF_N�CHSTEN_ZUG;
	}

	public final void figurBewegen() throws M�dnException {
		selectFigure();
		MoveValidationResult mrr = gl.getRules().moveValidation(this,
				currentStrategy);
		if (!mrr.isValid()) {
			currentStrategy = null;
			throw mrr.getError();
		}
		final Feld targetFeld = mrr.getTargetField();
		// Evtl. Gegner schmeissen
		if (targetFeld.getBesetzung() != null) {
			Feld gegnerischesStartfeld = targetFeld.getBesetzung()
					.getRelatedPlayer().getSp().getFreiesStartfeld();
			SpielFigur gegnerischeSpielfigur = targetFeld.getBesetzung();
			gegnerischeSpielfigur.setCurrentField(gegnerischesStartfeld);
			gegnerischesStartfeld.setBesetzung(gegnerischeSpielfigur);
		}
		// Figur bewegen
		currentStrategy.getCurrentField().setBesetzung(null);
		currentStrategy.setCurrentField(targetFeld);
		targetFeld.setBesetzung(currentStrategy);

		status = mrr.getNextPlayerState();
		currentStrategy = null;
	}

	public final boolean isFinished() {
		return gl.getRules().isFinished(this);
	}

	protected abstract void selectFigure() throws IllegalMoveException;

	public String getFarbe() {
		return farbe;
	}

	public Zielposition getZp() {
		return zp;
	}

	public Startposition getSp() {
		return sp;
	}

	public SpielerIngameStatus getStatus() {
		return status;
	}

	public void setStatus(SpielerIngameStatus status) {
		this.status = status;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Spieler;;;" + name + ","
				+ this.getClass().getSimpleName() + "," + farbe);
		if (this instanceof AiSpieler) {
			sb.append(","
					+ ((AiSpieler) this).getAi().getClass().getSimpleName());
		}
		sb.append(System.getProperty("line.separator"));
		for (ListIterator<SpielFigur> l = strategies.listIterator(); l
				.hasNext();) {
			sb.append(l.next().toString());

		}
		sb.append(this.getSp().toString());
		return sb.toString();
	}

	public GameLogic getGl() {
		return gl;
	}
}
