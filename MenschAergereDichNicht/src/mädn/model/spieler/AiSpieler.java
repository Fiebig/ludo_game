package m�dn.model.spieler;

import gametheory.ProfitFunction;
import m�dn.ai.Ai;
import m�dn.exceptions.IllegalMoveException;
import m�dn.logic.GameLogic;
import m�dn.model.spielbrett.Startposition;

public class AiSpieler extends M�dnSpieler {
	private static final long serialVersionUID = 1L;
	private final Ai ai;

	public AiSpieler(String name, String farbe, Startposition sp, GameLogic gl,
			ProfitFunction<M�dnSpieler, SpielFigur> pf, Ai ai) {
		super(name, farbe, sp, gl);
		this.ai = ai;
		this.profitComputer = pf;
	}

	@Override
	public void selectFigure() throws IllegalMoveException {
		if (currentStrategy != null) {
			throw new IllegalStateException(
					"Ai Spieler muss seine Figur automatisch aussuchen!");
		}
		currentStrategy = ai.computeBestFigure(this);
		if (currentStrategy == null) {
			throw new IllegalMoveException("Mit einer "
					+ gl.getW�rfel().getAktuelleAugenZahl() + " kann "
					+ this.name + " aktuell keine Figur bewegen!");
		}
	}

	public Ai getAi() {
		return ai;
	}

	@Override
	protected M�dnSpieler getThis() {
		return this;
	}

}
