package m�dn.model.spieler;

import gametheory.Strategy;

import java.util.List;

import m�dn.model.spielbrett.Feld;
import m�dn.model.spielbrett.Feldtyp;
import m�dn.model.spielbrett.Spielbrett;
import m�dn.model.spielbrett.Startposition;
import m�dn.model.spielbrett.Zielposition;
import m�dn.operationsContainer.OperationsContainer;

public class SpielFigur extends Strategy<SpielFigur, M�dnSpieler> {

	private static final long serialVersionUID = 1L;
	private Feld currentField;

	protected SpielFigur(M�dnSpieler spieler, Feld currentField) {
		super("SpielFigur;;; " + currentField.toString(), spieler);
		this.currentField = currentField;
	}

	public Feld getCurrentField() {
		return currentField;
	}

	public void setCurrentField(Feld currentField) {
		this.currentField = currentField;
	}

	public int getDistanceToHouse() {
		Zielposition zp = relatedPlayer.getZp();
		List<Feld> list = relatedPlayer.getGl().getSpielbrett()
				.getFieldsInOrderForZp(zp);
		Feld temp = currentField;
		if (temp.getFeldtyp() != Feldtyp.STARTFELD) {
			int distance = 0;
			while (temp.getFeldtyp() != Feldtyp.ZIELFELD) {
				temp = OperationsContainer.circleGet(list, temp, 1);
				distance++;
			}
			return distance;
		}
		return Integer.MAX_VALUE;
	}

	public boolean isNearlySaveOnField(Feld feld) {
		if (feld.getFeldtyp() != Feldtyp.STANDARDFELD) {
			return true;
		}
		Spielbrett sb = relatedPlayer.getGl().getSpielbrett();
		List<Feld> list = sb.getStandardFelderInOrder();

		for (Startposition sp : sb.getSps()) {
			if (!sp.isEmpty() && !sp.equals(relatedPlayer.getSp())) {
				Feld entryField = sb.getEntryFieldForSp(sp);
				if (feld.equals(entryField)) {
					return false;
				}
			}
		}
		Feld temp = feld;
		for (int i = 0; i < 6; i++) {
			temp = OperationsContainer.circleGet(list, temp, -1);
			if (temp.getBesetzung() != null
					&& !temp.getBesetzung().getRelatedPlayer()
							.equals(relatedPlayer)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public String toString() {
		return "SpielFigur;;;" + System.getProperty("line.separator")
				+ currentField.toString();
	}

}