package m�dn.model.spieler;

import m�dn.logic.GameLogic;
import m�dn.model.spielbrett.Startposition;

public class MenschlicherSpieler extends M�dnSpieler {

	private static final long serialVersionUID = 1L;

	public MenschlicherSpieler(String name, String farbe, Startposition sp,
			GameLogic gl) {
		super(name, farbe, sp, gl);
	}

	@Override
	protected void selectFigure() {
	}

	@Override
	protected M�dnSpieler getThis() {
		return this;
	}
}
