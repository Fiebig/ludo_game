package m�dn.model.w�rfel;

import java.io.Serializable;

public class W�rfel implements Serializable {

	private static final long serialVersionUID = 1L;
	private int aktuelleAugenZahl;

	public W�rfel() {
		this.aktuelleAugenZahl = (int) (Math.random() * 6 + 1);
	}

	public void neueAugenZahl() {
		this.aktuelleAugenZahl = (int) (Math.random() * 6 + 1);
	}

	public int getAktuelleAugenZahl() {
		return aktuelleAugenZahl;
	}

}
