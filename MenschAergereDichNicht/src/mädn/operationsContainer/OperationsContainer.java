package m�dn.operationsContainer;

import java.util.List;

import javafx.scene.paint.Color;

public class OperationsContainer {

	/**
	 * Liefert das Element, das position Eintr�ge relativ zum ausgangsElement im
	 * Kreis liegt. ArrayList sollte verwendet werden.
	 * 
	 * @param <E>
	 * @param ausgangsElement
	 *            Element ab dem gez�hlt wird
	 * @param position
	 *            Anzahl der Listeneintr�ge, die von ausgangsElement im Kreis
	 *            weitergegangen werden soll
	 * @return Element, das position Elemente enfernt von ausgangsElement im
	 *         Kreis liegt
	 */
	public static <E> E circleGet(List<E> list, E ausgangsElement, int position) {
		int index = list.lastIndexOf(ausgangsElement);
		E temp;
		if (position >= 0) {
			if ((index + position) > (list.size() - 1)) {
				temp = list.get((index + position) - list.size());
			}
			else {
				temp = list.get(index + position);
			}
		}
		else {
			if ((index + position) < 0) {
				temp = list.get((index + position) + list.size());
			}
			else {
				temp = list.get(index + position);
			}
		}
		return temp;
	}

	/**
	 * 
	 * @return
	 */
	public static Color randomColor() {
		int r = (int) (Math.random() * 255 + 1);
		int g = (int) (Math.random() * 255 + 1);
		int b = (int) (Math.random() * 255 + 1);
		return Color.rgb(r, g, b);
	}

	/**
	 * Wandelt eine JavaFx Color in eine Stringrepresentation um.
	 * 
	 * @param color
	 * @return
	 */
	public static String colorToHexString(Color color) {
		return String.format("#%02X%02X%02X", (int) (color.getRed() * 255),
				(int) (color.getGreen() * 255), (int) (color.getBlue() * 255));
	}

	public static void nFor(int depth, int low, int high, IAction action) {
		NestedFor nfor = new NestedFor(low, high, action);
		nfor.nFor(depth);
	}

	public static interface IAction {
		public void act(int[] indices);
	}

	private static class NestedFor {

		private final int lo;
		private final int hi;
		private final IAction action;

		private NestedFor(int lo, int hi, IAction action) {
			this.lo = lo;
			this.hi = hi;
			this.action = action;
		}

		private void nFor(int depth) {
			n_for(0, new int[0], depth);
		}

		private void n_for(int level, int[] indices, int maxLevel) {
			if (level == maxLevel) {
				action.act(indices);
			}
			else {
				int newLevel = level + 1;
				int[] newIndices = new int[newLevel];
				System.arraycopy(indices, 0, newIndices, 0, level);
				newIndices[level] = lo;
				while (newIndices[level] <= hi) {
					n_for(newLevel, newIndices, maxLevel);
					++newIndices[level];
				}
			}
		}

	}
}
